#!/bin/bash
JAVA_CLASSPATH="BOOT-INF/classes"
SECURITY_SERVER=$bamboo_xroadSecurityServer
CLIENT_XROAD_INSTANCE=$bamboo_clientXroadInstance
ADIT_REGISTERED_MEMBER=$bamboo_aditRegisteredMember
APP_VERSION=$bamboo_appVersion
jar="/app/bamboo-builders/java82/current/bin/jar"
$jar -xf adit-adapter-$APP_VERSION.jar
sed -i -e "/^security-server=/s|=.*|=$SECURITY_SERVER|" $JAVA_CLASSPATH/xroad.properties
sed -i -e "/^client-xroad-instance=/s|=.*|=$CLIENT_XROAD_INSTANCE|" $JAVA_CLASSPATH/xroad.properties
sed -i -e "/^adit-xroad-instance=/s|=.*|=$CLIENT_XROAD_INSTANCE|" $JAVA_CLASSPATH/xroad-adit.properties
sed -i -e "/^adit-registered-member=/s|=.*|=$ADIT_REGISTERED_MEMBER|" $JAVA_CLASSPATH/xroad-adit.properties
for file in xroad.properties xroad-adit.properties; do
  $jar -uf adit-adapter-$APP_VERSION.jar $JAVA_CLASSPATH/"$file"
done