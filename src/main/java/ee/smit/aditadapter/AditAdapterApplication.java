package ee.smit.aditadapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AditAdapterApplication {

    public static void main(String[] args) {
        SpringApplication.run(AditAdapterApplication.class, args);

        final String[] patterns = new String[]{".*"};
        //ResourceList.main(patterns);
    }

}
