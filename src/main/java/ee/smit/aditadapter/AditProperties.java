package ee.smit.aditadapter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class AditProperties {
    private static final Logger logger = LogManager.getLogger();

    @Value("${adit.service.name}")
    private String aditSystemReqName;

    public String getAditSystemReqName() {
        logger.debug("ADIT REGISTRED SYSTEM NAME: " + aditSystemReqName);
        return aditSystemReqName;
    }
}
