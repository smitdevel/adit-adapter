package ee.smit.aditadapter.app;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RestController
@RequestMapping("/app")
public class AppController {

    @Value("${info.build.version}")
    private String version;

    @Value("${application.name}")
    private String name;

    @Value("${group.id}")
    private String groupId;

    @GetMapping("")
    public Map<String, String> getAppInfo(){
        // Java 9
        //return Map.of("name",name, "version", version);

        // Java 8
        return Stream.of(new String[][] {
                { "name", name },
                { "group", groupId },
                { "version", version },
        }).collect(Collectors.toMap(data -> data[0], data -> data[1]));
    }

    @GetMapping("name")
    public String getName(){
        return name;
    }

    @GetMapping("version")
    public String getVersion(){
        return version;
    }

    @GetMapping("group")
    public String getGroupId() {
        return groupId;
    }
}
