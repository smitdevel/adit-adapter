package ee.smit.aditadapter.controller;

import ee.smit.aditadapter.AditProperties;
import ee.smit.aditadapter.domain.AditDocumentStatusInfoRequest;
import ee.smit.aditadapter.domain.AditSendStatusVIResponse;
import ee.smit.aditadapter.domain.AditUserInfoRequest;
import ee.smit.aditadapter.domain.AditUserInfoResponse;
import ee.smit.aditadapter.service.adit.XteeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping(path = "/api/xtee/adit")
@Tag(name = "Adit requests controller", description = "Handles requests")
public class AditRequestsController {
    private static final Logger logger = LogManager.getLogger();

    @Autowired
    XteeService service;

    @Autowired
    AditProperties properties;

    @Operation(summary = "Gets user info", description = "User info search by ids", tags = {"request"},
            responses = {
                    @ApiResponse(responseCode = "400", description = "Input JSON request is null!"),
                    @ApiResponse(responseCode = "400", description = "System id is missing from request!"),
                    @ApiResponse(responseCode = "400", description = "User id code set is missing from request!"),
                    @ApiResponse(responseCode = "400", description = "User id code set is empty!"),
            }) //Hetkel võtab swagger viimase apiresponse, sest kõigi response koodid on controlleris samad.
    // Kui oleks näiteks 400, 401 jne, siis kuvaks swagger kõik 4 võimalikku response'i
    @PostMapping("getUserInfo")
    public AditUserInfoResponse getUserInfo(@RequestBody AditUserInfoRequest input) {
        logger.debug("Get user info...");
        AditUserInfoResponse response = new AditUserInfoResponse();
        if (input == null) {
            response.setStatusCode(400);
            response.setStatus("Input JSON request is null!");
            return response;
        }

        if (input.getSystemId() == null) {
            response.setStatusCode(400);
            response.setStatus("System id is missing from request!");
            return response;
        }

        if (input.getUserids() == null) {
            response.setStatusCode(400);
            response.setStatus("User id code set is missing from request!");
            return response;
        }

        if (input.getUserids().size() == 0) {
            response.setStatusCode(400);
            response.setStatus("User id code set is empty!");
            return response;
        }

        return service.getUserInfoStatuses(input, properties.getAditSystemReqName());
    }

    @Operation(summary = "Get user info example", description = "Example post request", tags = {"example"})
    @GetMapping("getExampleRequest")
    public AditUserInfoRequest getExample() {
        AditUserInfoRequest req = new AditUserInfoRequest();
        req.setSystemId("70008440");
        req.setUserIdCode("37701010001");
        Set<String> userids = Set.of("39010101234", "48012124321");
        req.setUserids(userids);
        return req;
    }

    @Operation(summary = "Gets send status on info request", description = "Send status V1", tags = {"status"})
    @PostMapping("getSendStatusV1")
    public AditSendStatusVIResponse getSendStatusV1(@RequestBody
                                                            AditDocumentStatusInfoRequest input) {

        return service.getSendStatusV1(input, properties.getAditSystemReqName());
    }
}
