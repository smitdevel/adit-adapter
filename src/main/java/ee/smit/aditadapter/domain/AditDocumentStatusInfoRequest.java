package ee.smit.aditadapter.domain;

import java.util.Set;

public class AditDocumentStatusInfoRequest extends AditStatusDefaultRequest {

    Set<String> docIds;

    public Set<String> getDocIds() {
        return docIds;
    }

    public void setDocIds(Set<String> docIds) {
        this.docIds = docIds;
    }
}
