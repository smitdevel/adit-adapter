package ee.smit.aditadapter.domain;

import java.util.List;
import java.util.Map;

public class AditSendStatusVIResponse extends DefaultResponse {

    Map<String, List<AditDocSendStatusInfo>> documents;

    public Map<String, List<AditDocSendStatusInfo>> getDocuments() {
        return documents;
    }

    public void setDocuments(Map<String, List<AditDocSendStatusInfo>> documents) {
        this.documents = documents;
    }


}
