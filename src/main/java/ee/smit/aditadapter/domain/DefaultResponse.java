package ee.smit.aditadapter.domain;

public class DefaultResponse {

    private int statusCode;

    private String status;

    public int getStatusCode() {
        return statusCode;
    }

    // -- GETTERS, SETTERS -------------------------------
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
