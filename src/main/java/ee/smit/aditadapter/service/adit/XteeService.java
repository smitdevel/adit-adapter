package ee.smit.aditadapter.service.adit;

import ee.smit.aditadapter.domain.AditDocumentStatusInfoRequest;
import ee.smit.aditadapter.domain.AditSendStatusVIResponse;
import ee.smit.aditadapter.domain.AditUserInfoRequest;
import ee.smit.aditadapter.domain.AditUserInfoResponse;

import java.util.Set;

public interface XteeService {

    String BEAN_NAME = "AditService";
    String NAME = "adit";

    AditUserInfoResponse getUserInfoStatuses(AditUserInfoRequest info, String aditReqName);

    AditUserInfoResponse getUnregisteredAditUsers(Set<String> userIdCodes, String userIdCode, String aditSystemName, String systemRegCode);

    AditSendStatusVIResponse getSendStatusV1(AditDocumentStatusInfoRequest info, String aditReqName);

    AditSendStatusVIResponse updateAditDocViewedStatuses(Set<String> userIdCodes, String reqUserIdCode, String aditSystemName, String systemRegCode);

}
