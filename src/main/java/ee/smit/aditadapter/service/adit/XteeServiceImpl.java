package ee.smit.aditadapter.service.adit;

import com.nortal.jroad.client.adit.AditProp;
import com.nortal.jroad.client.adit.AditXTeeService;
import com.nortal.jroad.client.exception.XRoadServiceConsumptionException;
import com.nortal.jroad.client.service.configuration.provider.PropertiesBasedXRoadServiceConfigurationProvider;
import ee.smit.aditadapter.domain.AditDocSendStatusInfo;
import ee.smit.aditadapter.domain.AditDocumentStatusInfoRequest;
import ee.smit.aditadapter.domain.AditSendStatusVIResponse;
import ee.smit.aditadapter.domain.AditUserInfo;
import ee.smit.aditadapter.domain.AditUserInfoRequest;
import ee.smit.aditadapter.domain.AditUserInfoResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class XteeServiceImpl implements XteeService {
    private static final Logger logger = LogManager.getLogger();

    @Autowired
    ApplicationContext applicationContext;
    @Autowired
    PropertiesBasedXRoadServiceConfigurationProvider xRoadServiceConfigurationProvider;
    @Autowired
    private AditXTeeService service;

    /**
     * Returns AditUserInfoResponse object that contains list of active users, list of unregistered users and a message.
     *
     * @param info        UserInfoRequest with user ids
     * @param aditReqName Request name
     * @return active users, unregistered users and a message
     */
    public AditUserInfoResponse getUserInfoStatuses(AditUserInfoRequest info, String aditReqName) {
        logger.info("getUserInfoStatuses REQUEST... USER ID-code: "
                + info.getUserIdCode()
                + "; SYSTEM REG NR: " + info.getSystemId()
                + "; ADIT REG SYSTEM NAME: " + aditReqName
                + "; USER IDS: " + info.getUserids());

        return getUnregisteredAditUsers(info.getUserids(), info.getUserIdCode(), aditReqName, info.getSystemId());
    }

    /**
     * Checks if users are registered within Adit, whether they are active or not and sets statuses accordingly.
     *
     * @param userIdCodes    Set of user id codes
     * @param reqUserIdCode  Requesting user id code
     * @param aditSystemName Adit system name
     * @param systemRegCode  System's regional code
     * @return statuses
     */
    public AditUserInfoResponse getUnregisteredAditUsers(Set<String> userIdCodes, String reqUserIdCode,
                                                         String aditSystemName, String systemRegCode) {
        AditUserInfoResponse statuses = new AditUserInfoResponse();
        List<AditUserInfo> activeUsers = new ArrayList<>();
        List<AditUserInfo> deactiveUsers = new ArrayList<>();
        try {
            logger.debug("Request user ID-Code: " + reqUserIdCode + "; ADIT systemName: "
                    + aditSystemName + "; System Reg.code: " + systemRegCode);

            Map<String, Map<String, Serializable>> results = service.getUserInfoV1(userIdCodes, aditSystemName);
            if (!results.isEmpty()) {
                for (String userId : userIdCodes) {
                    logger.debug("USER ID-code: " + userId);

                    AditUserInfo aditUserInfo = new AditUserInfo();
                    aditUserInfo.setUserIdCode(userId);

                    Map<String, Serializable> props = results.get(userId);
                    if (props == null) {
                        logger.debug("No userinfo found! Add to unreqistered user list. Continue..");
                        deactiveUsers.add(aditUserInfo);
                        continue;
                    }

                    boolean hasJoined = Boolean.TRUE.equals(props.get(AditProp.User.HAS_JOINED.getName()));
                    logger.debug("Has user [" + userId + "] has joined with ADIT: " + hasJoined);
                    boolean usesDvk = Boolean.TRUE.equals(props.get(AditProp.User.USES_DVK.getName()));
                    logger.debug("User [" + userId + "] uses DVK: " + usesDvk);
                    boolean canRead = Boolean.TRUE.equals(props.get(AditProp.User.CAN_READ.getName()));
                    logger.debug("User [" + userId + "] can read: " + canRead);
                    boolean canWrite = Boolean.TRUE.equals(props.get(AditProp.User.CAN_WRITE.getName()));
                    logger.debug("User [" + userId + "] can write: " + canWrite);

                    aditUserInfo.setCanRead(canRead);
                    aditUserInfo.setCanWrite(canWrite);
                    aditUserInfo.setHasJoinded(hasJoined);

                    if (hasJoined) {
                        aditUserInfo.setMessages("OK!");
                        activeUsers.add(aditUserInfo);
                    } else {
                        aditUserInfo.setMessages("Isik ei ole teenusega ADIT liitunud.");
                        deactiveUsers.add(aditUserInfo);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            statuses.setMessage(e.toString());
            statuses.setStatusCode(400);
            statuses.setStatus("Request failed!");
        }

        statuses.setStatusCode(200);
        statuses.setStatus("Successful!");
        statuses.setActiveUserList(activeUsers);
        statuses.setUnreqistredUserList(deactiveUsers);
        statuses.setMessage("OK!");
        return statuses;
    }

    /**
     * Updates document viewed statuses.
     *
     * @param info        info request containing user id code, system id, request name and document ids
     * @param aditReqName request name
     * @return updated document viewing statuses
     */
    public AditSendStatusVIResponse getSendStatusV1(AditDocumentStatusInfoRequest info, String aditReqName) {
        logger.info("getSendStatus REQUEST... USER ID-code: "
                + info.getUserIdCode()
                + "; SYSTEM REG NR: " + info.getSystemId()
                + "; ADIT REG SYSTEM NAME: " + aditReqName
                + "; DOC IDS object: " + info.getDocIds());
        return updateAditDocViewedStatuses(info.getDocIds(), info.getUserIdCode(), aditReqName, info.getSystemId());
    }

    /**
     * Checks documents, their opening times and user response.
     *
     * @param docIds         document id's to be checked
     * @param reqUserIdCode  requesting user id code
     * @param aditSystemName system name
     * @param systemRegCode  system's regional code
     * @return response containing documents and statuses
     */
    public AditSendStatusVIResponse updateAditDocViewedStatuses(Set<String> docIds, String reqUserIdCode,
                                                                String aditSystemName, String systemRegCode) {
        AditSendStatusVIResponse response = new AditSendStatusVIResponse();

        Map<String, List<Map<String, Serializable>>> sendStatuses;
        logger.debug("Try....");
        List<String> cloneIdList = convertSetToList(docIds);
        try {

            sendStatuses = service.getSendStatusV1(docIds, aditSystemName);
            logger.debug("Response...successful!");
        } catch (RuntimeException e) {
            response.setStatus(e.getMessage());
            response.setStatusCode(400);
            return response;
        } catch (XRoadServiceConsumptionException e) {
            String faultMessage =
                    e.getNonTechnicalFaultString() != null ? e.getNonTechnicalFaultCode() : e.getFaultString();
            response.setStatus(faultMessage);
            response.setStatusCode(400);
            return response;
        }

        logger.debug("Start checking response statuses....");
        Map<String, List<AditDocSendStatusInfo>> map = new HashMap<>();

        logger.debug("Document id list size: " + cloneIdList.size());

        for (String dvkId : cloneIdList) {
            logger.debug("Document dvk/dhx id: " + dvkId);
            List<AditDocSendStatusInfo> infoList = new ArrayList<>();


            List<Map<String, Serializable>> personPropslist = sendStatuses.get(dvkId);
            if (personPropslist == null || personPropslist.size() == 0) {
                AditDocSendStatusInfo info = new AditDocSendStatusInfo();
                logger.debug("Document not found!");
                info.setDocId(dvkId);
                info.setMessage("Document not found!");
                info.setStatusCode(400);
                infoList.add(info);
                map.put(dvkId, infoList);
                continue;
            }
            logger.debug("User response LIST size: " + personPropslist.size());

            int i = 0;
            for (Map<String, Serializable> personProps : personPropslist) {
                i++;
                logger.debug("---- List item " + i);
                AditDocSendStatusInfo info = new AditDocSendStatusInfo();
                info.setDocId(dvkId);
                String userCode = (String) personProps.get(AditProp.Document.ID_CODE.getName());
                info.setUserCode(userCode);
                String receiverName = (String) personProps.get(AditProp.Document.REVEICER_NAME.getName());
                info.setReceiverName(receiverName);
                boolean containsOpenTime = personProps.containsKey(AditProp.Document.OPEN_TIME.getName());
                info.setContainsOpenTime(containsOpenTime);
                Date openTime = (Date) personProps.get(AditProp.Document.OPEN_TIME.getName());
                info.setOpenTime(openTime);
                boolean opened = (boolean) personProps.get(AditProp.Document.OPENED.getName());
                info.setOpened(opened);
                info.setStatusCode(200);
                infoList.add(info);


                for (Map.Entry<String, Serializable> entry : personProps.entrySet()) {
                    logger.debug(entry.getKey());
                }
            }
            map.put(dvkId, infoList);
        }

        response.setDocuments(map);
        response.setStatusCode(200);
        response.setStatus("OK!");
        return response;
    }

    private List<String> convertSetToList(Set<String> ids) {
        logger.debug("Convert SET TO LIST...");
        List<String> list = new ArrayList<>();
        for (String id : ids) {
            logger.debug("ID: " + id);
            list.add(id);
        }
        return list;
    }

    /**
     * Sets mocked XTeeService, necessary for unit test.
     *
     * @param service mocked service
     */
    public void setService(AditXTeeService service) {
        this.service = service;
    }
}
