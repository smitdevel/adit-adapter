package ee.smit.aditadapter.controller;

import com.google.gson.Gson;
import ee.smit.aditadapter.domain.AditUserInfoRequest;
import ee.smit.aditadapter.domain.AditUserInfoResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AditRequestsController.class)
public class AditRequestsControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @MockBean
    private AditRequestsController controller;

    private Gson gson;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).dispatchOptions(true).build();
        gson = new Gson();
        controller = new AditRequestsController();
    }

    @Test
    public void getUserInfoInputNull() throws Exception {
        AditUserInfoRequest input = null;

        AditUserInfoResponse response = new AditUserInfoResponse();
        response.setStatus("Input JSON request is null!");
        response.setStatusCode(400);

        String inputJson = gson.toJson(input);

        RequestBuilder request = MockMvcRequestBuilders
                .post("/api/xtee/adit/getUserInfo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        mockMvc.perform(request)
                .andReturn()
                .getResponse();

        assertEquals("Input JSON request is null!", controller.getUserInfo(input).getStatus());
        assertEquals(400, controller.getUserInfo(input).getStatusCode());
    }

    @Test
    public void getUserInfoSystemIDNull() throws Exception {
        AditUserInfoRequest input = new AditUserInfoRequest();
        input.setUserids(Set.of("11111", "22222"));
        input.setSystemId(null);
        input.setUserIdCode("666666");

        AditUserInfoResponse response = new AditUserInfoResponse();
        response.setStatus("System id is missing from request!");
        response.setStatusCode(400);

        String inputJson = gson.toJson(input);

        RequestBuilder request = MockMvcRequestBuilders
                .post("/api/xtee/adit/getUserInfo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertEquals("System id is missing from request!", controller.getUserInfo(input).getStatus());
        assertEquals(400, controller.getUserInfo(input).getStatusCode());
    }

    @Test
    public void getUserInfoUserIDsNull() throws Exception {
        AditUserInfoRequest input = new AditUserInfoRequest();
        input.setSystemId("1");
        input.setUserIdCode("666666");
        input.setUserids(null);

        AditUserInfoResponse response = new AditUserInfoResponse();
        response.setStatus("User id code set is missing from request!");
        response.setStatusCode(400);

        String inputJson = gson.toJson(input);

        RequestBuilder request = MockMvcRequestBuilders
                .post("/api/xtee/adit/getUserInfo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertEquals("User id code set is missing from request!", controller.getUserInfo(input).getStatus());
        assertEquals(400, controller.getUserInfo(input).getStatusCode());
    }

    @Test
    public void getUserInfoUserIDSetIsEmptyNull() throws Exception {
        AditUserInfoResponse response = new AditUserInfoResponse();
        response.setStatus("User id code set is empty!");
        response.setStatusCode(400);

        AditUserInfoRequest input = new AditUserInfoRequest();
        input.setUserids(new HashSet<>());
        input.setSystemId("12");
        input.setUserIdCode("666666");

        String inputJson = gson.toJson(input);

        RequestBuilder request = MockMvcRequestBuilders
                .post("/api/xtee/adit/getUserInfo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson);

        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertEquals("User id code set is empty!", controller.getUserInfo(input).getStatus());
        assertEquals(400, controller.getUserInfo(input).getStatusCode());
    }
}
