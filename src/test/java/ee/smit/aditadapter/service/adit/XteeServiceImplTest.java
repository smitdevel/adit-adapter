package ee.smit.aditadapter.service.adit;

import com.nortal.jroad.client.adit.AditProp;
import com.nortal.jroad.client.adit.AditXTeeService;
import com.nortal.jroad.client.exception.NonTechnicalFaultException;
import com.nortal.jroad.client.exception.XRoadServiceConsumptionException;
import ee.smit.aditadapter.domain.*;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

public class XteeServiceImplTest {
    private final AditUserInfoRequest aditUserInfoRequest = new AditUserInfoRequest();
    private final AditXTeeService service = mock(AditXTeeService.class);
    private static final String TEST_ISIKUKOOD = "386062791";
    private Set<String> ids;

    @Resource
    XteeServiceImpl xteeServiceImpl = new XteeServiceImpl();

    @Before
    public void setUp() {
        xteeServiceImpl.setService(service);

        aditUserInfoRequest.setUserIdCode("123456789");
        aditUserInfoRequest.setSystemId("15432");
        ids = Set.of("3333333", "4444444", "5555555");
        aditUserInfoRequest.setUserids(ids);
    }

    @Test
    public void getUserInfoStatuses() throws XRoadServiceConsumptionException {
        Map<String, Map<String, Serializable>> results = new HashMap<>();
        Map<String, Serializable> props = new HashMap<>();

        props.put(AditProp.User.HAS_JOINED.getName(), true);
        props.put(AditProp.User.CAN_READ.getName(), true);
        props.put(AditProp.User.CAN_WRITE.getName(), false);
        props.put(AditProp.User.USES_DVK.getName(), false);
        results.put("3333333", props);

        Map<String, Serializable> props2 = new HashMap<>();
        props2.put(AditProp.User.HAS_JOINED.getName(), true);
        props2.put(AditProp.User.CAN_READ.getName(), true);
        props2.put(AditProp.User.CAN_WRITE.getName(), true);
        props2.put(AditProp.User.USES_DVK.getName(), true);
        results.put("4444444", props2);

        results.put("5555555", null);

        when(service.getUserInfoV1(ids, "15432")).thenReturn(results);
        AditUserInfoResponse response = xteeServiceImpl.getUserInfoStatuses(aditUserInfoRequest, "15432");

        assertEquals(response.getClass(), AditUserInfoResponse.class);
        assertEquals(1, response.getUnreqistredUserList().size());
        assertEquals("OK!", response.getMessage());
        assertEquals(200, response.getStatusCode());
    }

    @Test
    public void updateAditDocViewedStatusesThrowsException() throws XRoadServiceConsumptionException {
        XRoadServiceConsumptionException exception =
                new XRoadServiceConsumptionException(new NonTechnicalFaultException("error",
                        "non technical fault exception"), "xroad", "update adit doc viewed statuses", "1.0.0");
        when(service.getSendStatusV1(ids, "adit")).thenThrow(exception);

        assertEquals("error", xteeServiceImpl.updateAditDocViewedStatuses(ids,
                TEST_ISIKUKOOD, "adit", "15432").getStatus());
    }

    @Test
    public void updateAditDocViewedStatuses() throws XRoadServiceConsumptionException {
        Map<String, List<Map<String, Serializable>>> sendStatuses = new HashMap<>();
        List<Map<String, Serializable>> personPropslist = new ArrayList<>();
        Map<String, Serializable> personMap = new HashMap<>();

        personMap.put(AditProp.Document.ID_CODE.getName(), "3333333");
        personMap.put(AditProp.Document.REVEICER_NAME.getName(), "tom");
        personMap.put(AditProp.Document.OPEN_TIME.getName(), true);
        personMap.put(AditProp.Document.OPEN_TIME.getName(), LocalDate.of(2020, 6, 25));
        personMap.put(AditProp.Document.OPENED.getName(), false);
        personPropslist.add(personMap);

        personMap.clear();
        personMap.put(AditProp.Document.ID_CODE.getName(), "4444444");
        personMap.put(AditProp.Document.REVEICER_NAME.getName(), "tim");
        personMap.put(AditProp.Document.OPEN_TIME.getName(), true);
        personMap.put(AditProp.Document.OPEN_TIME.getName(), LocalDate.of(2020, 6, 23));
        personMap.put(AditProp.Document.OPENED.getName(), true);
        personPropslist.add(personMap);

        sendStatuses.put("1", personPropslist);
        personPropslist.clear();
        personMap.clear();
        personMap.put(AditProp.Document.ID_CODE.getName(), "5555555");
        personMap.put(AditProp.Document.REVEICER_NAME.getName(), "tam");
        personMap.put(AditProp.Document.OPEN_TIME.getName(), false);
        personMap.put(AditProp.Document.OPEN_TIME.getName(), LocalDate.of(2020, 6, 22));
        personMap.put(AditProp.Document.OPENED.getName(), false);
        personPropslist.add(personMap);
        sendStatuses.put("2", personPropslist);

        when(service.getSendStatusV1(ids, "adit")).thenReturn(sendStatuses);

        AditSendStatusVIResponse response = xteeServiceImpl.updateAditDocViewedStatuses(Set.of("1", "2"),
                TEST_ISIKUKOOD, "adit", "15432");
        assertEquals(2, response.getDocuments().size());

        AditDocumentStatusInfoRequest info = new AditDocumentStatusInfoRequest();
        info.setUserIdCode(TEST_ISIKUKOOD);
        info.setSystemId("15432");
        info.setDocIds(Set.of("1", "2"));

        assertEquals(AditSendStatusVIResponse.class, xteeServiceImpl.getSendStatusV1(info, "adit").getClass());
    }

}
